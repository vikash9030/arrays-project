function flatten(elements) {
    let flattenArr = [];
    elements.forEach(el => {
      if(Array.isArray(el)){
        const result = flatten(el);
        result.forEach(el => flattenArr.push(el));
      } else {
        flattenArr.push(el);
      }
    });
    return flattenArr;
}

 module.exports = flatten;