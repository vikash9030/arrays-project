function find(elements,cb){
    for(let index = 0;index<elements.length;index++){
        let value = cb(elements[index]);

        if(value == true){
            return elements[index];
        }
    }
    return undefined;
}
module.exports =find;