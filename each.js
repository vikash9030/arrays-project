function each(elements , cb){
    let newArray = []
    for(let index=0;index<elements.length;index++){
        newArray.push(cb(elements[index]));
    }
   return newArray;
}
module.exports = each;