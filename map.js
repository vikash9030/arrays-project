function map (element , callback) {
    const resultArray = [];
    for (let index = 0; index < element.length; index++) {
        resultArray.push(callback(element[index]));
    }
    return resultArray;
}


module.exports = map;
//const items = [1, 2, 3, 4, 5, 5];

