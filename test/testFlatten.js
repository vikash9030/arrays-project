const flatten = require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]];

const resultArr = flatten(nestedArray);

console.log(resultArr);