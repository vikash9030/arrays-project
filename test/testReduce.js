const reduce= require("../reduce.js");
var elements = [1, 2, 3, 4, 5, 5];
function sumArray(elements,startingValue){
    let sum=0;
    if(startingValue === undefined){
        
        for(var index = 0 ; index<elements.length ; index++){
            sum += elements[index];

        }

    }else{
       
        for(var index = startingValue ; index<elements.length ; index++){
            sum +=elements[index];

        }

    }
    return sum;
}
startingValue = 2;
var ans = reduce(elements ,sumArray,startingValue);

console.log(ans);